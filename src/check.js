const shell = require("shelljs");
const http = require("http");
const { exec } = require("child_process");
const { rm } = require("fs");

if (!shell.which("git")) {
  shell.echo("Sorry! this script requires Git");
}

const brand = process.env.BRAND_NAME;
const locale = process.env.BRAND_LOCALE;
const version = process.env.ROLLOUT_VERSION;

const sourcebranchName = process.env.AIBA_SOURCE_BRANCH;
const bitbucketUser = process.env.BITBUCKET_USER;
const bitbucketPasswordSecret = process.env.BITBUCKET_PASSWORD_SECRET;

const STAGE_API_URL_AIBA_SERVICES = process.env.STAGE_API_URL_AIBA_SERVICES;
const STAGE_API_KEY_AIBA_SERVICES = process.env.STAGE_API_KEY_AIBA_SERVICES;

const STAGE_AWS_ACCESS_KEY_ID = process.env.STAGE_AWS_ACCESS_KEY_ID;
const STAGE_AWS_SECRET_ACCESS_KEY = process.env.STAGE_AWS_SECRET_ACCESS_KEY;
const STAGE_S3_BUCKET_NAME = process.env.STAGE_S3_BUCKET_NAME;

const STAGE_AKAMAI_BASE_URL = process.env.STAGE_AKAMAI_BASE_URL;
const STAGE_AKAMAI_API_PATH = process.env.STAGE_AKAMAI_API_PATH;
const STAGE_AKAMAI_CLIENT_TOKEN = process.env.STAGE_AKAMAI_CLIENT_TOKEN;
const STAGE_AKAMAI_ACCESS_TOKEN = process.env.STAGE_AKAMAI_ACCESS_TOKEN;
const STAGE_AKAMAI_CLIENT_SECRET = process.env.STAGE_AKAMAI_CLIENT_SECRET;

// const BASE_URL = `https://uat.aiba.unileverqa.com/${STAGE_S3_REMOTE_PATH}`;

const branchName = `${brand}-${locale}-${version}`;
const workspace = "avinash_digital20_platform";

console.log("Branch Name :", branchName);
console.log("Create brand repo :", branchName);

const pipelineEnvVariables = {
  aibaApiUrl: {
    key: "STAGE_API_URL_AIBA_SERVICES",
    value: `${STAGE_API_URL_AIBA_SERVICES}`,
    secured: "true",
  },
  aibaApiKey: {
    key: "STAGE_API_KEY_AIBA_SERVICES",
    value: `${STAGE_API_KEY_AIBA_SERVICES}`,
    secured: "true",
  },
  awsAccessKey: {
    key: "STAGE_AWS_ACCESS_KEY_ID",
    value: `${STAGE_AWS_ACCESS_KEY_ID}`,
    secured: "true",
  },
  awsSecretAccessKey: {
    key: "STAGE_AWS_SECRET_ACCESS_KEY",
    value: `${STAGE_AWS_SECRET_ACCESS_KEY}`,
    secured: "true",
  },
  s3BucketName: {
    key: "STAGE_S3_BUCKET_NAME",
    value: `${STAGE_S3_BUCKET_NAME}`,
    secured: "true",
  },
  akamaiBaseUrl: {
    key: "STAGE_AKAMAI_BASE_URL",
    value: `${STAGE_AKAMAI_BASE_URL}`,
    secured: "true",
  },
  akamaiApiPath: {
    key: "STAGE_AKAMAI_API_PATH",
    value: `${STAGE_AKAMAI_API_PATH}`,
    secured: "true",
  },
  akamaiClientToken: {
    key: "STAGE_AKAMAI_CLIENT_TOKEN",
    value: `${STAGE_AKAMAI_CLIENT_TOKEN}`,
    secured: "true",
  },
  akamaiAccessToken: {
    key: "STAGE_AKAMAI_ACCESS_TOKEN",
    value: `${STAGE_AKAMAI_ACCESS_TOKEN}`,
    secured: "true",
  },
  akamaiClientSecret: {
    key: "STAGE_AKAMAI_CLIENT_SECRET",
    value: `${STAGE_AKAMAI_CLIENT_SECRET}`,
    secured: "true",
  },
};

// Repo/Branch Config
const branchConfigUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}`;
const branchConfigOptions = {
  method: "POST",
  url: branchConfigUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    scm: "git",
    is_private: "true",
    project: {
      key: "AIBA",
    },
  }),
};
http.request(branchConfigOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// Pipeline Config
console.log("Enable pipeline in brand repo");
const pipelineConfigUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config`;
const pipelineConfigOptions = {
  method: "PUT",
  url: pipelineConfigUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    enabled: "true",
  }),
};
http.request(pipelineConfigOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// Set Repo Variables
console.log("Set Repo level variables");

// AIBA API URL
const setPipelineVariablesUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAibaApiUrlOptions = {
  method: "POST",
  url: setAibaApiUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_API_URL_AIBA_SERVICES",
    value: `${STAGE_API_URL_AIBA_SERVICES}`,
    secured: "true",
  }),
};
http.request(setAibaApiUrlOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// AIBA API KEY
const setAibaApiKeyUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAibaApiKeyOptions = {
  method: "POST",
  url: setAibaApiKeyUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_API_KEY_AIBA_SERVICES",
    value: `${STAGE_API_KEY_AIBA_SERVICES}`,
    secured: "true",
  }),
};
http.request(setAibaApiKeyOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// AWS Access Key
const setAWSAccessKeyUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAWSAccessKeyOptions = {
  method: "POST",
  url: setAWSAccessKeyUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_AWS_ACCESS_KEY_ID",
    value: `${STAGE_AWS_ACCESS_KEY_ID}`,
    secured: "true",
  }),
};
http.request(setAWSAccessKeyOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// AWS Secret Key
const setAWSSecretUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAWSSecretOptions = {
  method: "POST",
  url: setAWSSecretUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_AWS_SECRET_ACCESS_KEY",
    value: `${STAGE_AWS_SECRET_ACCESS_KEY}`,
    secured: "true",
  }),
};
http.request(setAWSSecretOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// S3 Bucket Name
const setS3BucketNameUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setS3BucketNameOptions = {
  method: "POST",
  url: setS3BucketNameUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_S3_BUCKET_NAME",
    value: `${STAGE_S3_BUCKET_NAME}`,
    secured: "true",
  }),
};
http.request(setS3BucketNameOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// Akamai Base URL
const setAkamaiBaseUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAkamaiBaseUrlOptions = {
  method: "POST",
  url: setAkamaiBaseUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_AKAMAI_BASE_URL",
    value: `${STAGE_AKAMAI_BASE_URL}`,
    secured: "true",
  }),
};
http.request(setAkamaiBaseUrlOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// Akamai API path
const setAkamaiApiPathUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAkamaiApiPathOptions = {
  method: "POST",
  url: setAkamaiApiPathUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_AKAMAI_API_PATH",
    value: `${STAGE_AKAMAI_API_PATH}`,
    secured: "true",
  }),
};
http.request(setAkamaiApiPathOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// Akamai Client token
const setAkamaiClientTokenUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAkamaiClientTokenOptions = {
  method: "POST",
  url: setAkamaiClientTokenUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_AKAMAI_CLIENT_TOKEN",
    value: `${STAGE_AKAMAI_CLIENT_TOKEN}`,
    secured: "true",
  }),
};
http.request(setAkamaiClientTokenOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// Akamai Access token
const setAkamaiAccessTokenUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAkamaiAccessTokenOptions = {
  method: "POST",
  url: setAkamaiAccessTokenUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_AKAMAI_ACCESS_TOKEN",
    value: `${STAGE_AKAMAI_ACCESS_TOKEN}`,
    secured: "true",
  }),
};
http.request(setAkamaiAccessTokenOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

// Akamai Client Secret
const setAkamaiClientSecretUrl = `https://${bitbucketUser}:${bitbucketPasswordSecret}@api.bitbucket.org/2.0/repositories/${workspace}/${branchName}/pipelines_config/variables/`;
var setAkamaiClientSecretOptions = {
  method: "POST",
  url: setAkamaiClientSecretUrl,
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    key: "STAGE_AKAMAI_CLIENT_SECRET",
    value: `${STAGE_AKAMAI_CLIENT_SECRET}`,
    secured: "true",
  }),
};
http.request(setAkamaiClientSecretOptions, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

console.log(" List of files ");
exec("ls -ltr");
exec("git init");
exec(`git config --global user.name ${bitbucketUser}`);
exec(`git config --global user.password ${bitbucketPasswordSecret}`);
exec(
  'git config --global user.email "aiba_support_pbs_team_ind@publicissapient.com"'
);

console.log("Source Repo checkout");

exec(
  `git clone --branch ${sourcebranchName} https://${bitbucketUser}:${bitbucketPasswordSecret}@bitbucket.org/${workspace}/aiba-microfrontend.git`
);
exec("cd aiba-microfrontend");
exec(`git checkout ${sourcebranchName}`);
exec("git pull");

console.log("List of the files in Source Repo");
exec("ls -ltr");

// Deleting the files and folders which are not required for the rolled out repo
rm("-f", "bitbucket-pipelines.yml");
rm("-rf", "packages/widget-api-tests/");
rm("-rf", "packages/widget-ui-configurator/");
exec("mv branddeploy.yml bitbucket-pipelines.yml");
cd("..");

console.log("Dest Repo Checkout");
exec(
  `git clone https://${bitbucketUser}:${bitbucketPasswordSecret}@bitbucket.org/${workspace}/${branchName}.git`
);
console.log(`Copy the files in the ${branchName} repo`);
exec(`cp ./aiba-microfrontend/.gitignore ./${branchName}`);
exec(`cp ./aiba-microfrontend/.prettierignore ./${branchName}`);
exec(`cp -rv ./aiba-microfrontend/* ./${branchName}`);
cd(`${branchName}`);

console.log(
  `List of files in $branchName repo after copy from source repo branch ${sourcebranchName}`
);
exec("ls -ltr");

// Update the configurator file before check in the code
console.log("Update the configurator file before check in the code");
exec("git add .");
exec("git status");
exec('git commit -m "Initial code commit"');
console.log("git branch");
exec("git branch");
console.log("git remote -v");
exec("git remote -v");
console.log("git push origin master");
exec("git push origin master");
exec("ls -ltr");
