#!/bin/bash
######################################################################################################################################################
#Script Name	: create-brand-repo.sh <brand Name> <locale Name> <version number> <source Branch Name> <BITBUCKET_USER>
#                 <BITBUCKET_PASSWORD_SECRET> <AWS_ACCESS_KEY_ID> <AWS_SECRET_ACCESS_KEY> <S3_REMOTE_PATH>
######################################################################################################################################################
#Args pass check
if [ $# -ne 13 ]; then
    echo "$0: usage: $0 <brand Name> <locale Name> <version number> <source Branch Name> <BITBUCKET_USER> <BITBUCKET_PASSWORD_SECRET> \
    <AWS_ACCESS_KEY_ID>  <AWS_SECRET_ACCESS_KEY> <S3_BUCKET_NAME> <S3_BUCKET_REGION>"
    exit 1
fi
echo "Run shell script"
brand=$1
locale=$2
version=$3
sourceBranchName=$4
bitbucketUser=$5
bitbucketPasswordSecret=$6
AWS_ACCESS_KEY_ID=$7
AWS_SECRET_ACCESS_KEY=$8
REVIEVE_PARTNER_ID=$9
FORM_V3_API_KEY=$10
AIBA_SERVICES_API_KEY=$11
S3_BUCKET_NAME=$12
S3_BUCKET_REGION=$13
S3_REMOTE_PATH="$brand/$locale/$version"
BASE_URL="https://uat.aiba.unileverqa.com/$S3_REMOTE_PATH"
branchname="$brand-$locale-$version"
workspace="avinash_digital20_platform"
echo "OS version"
uname -a
echo "Branch Name : $branchname"
echo "##Create brand repo :: $branchname"
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "scm": "git",
    "is_private": "true",
    "project": {
        "key": "AIBA"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname
echo "##Enable pipeline in brand repo##"
curl -u $bitbucketUser:$bitbucketPasswordSecret -X PUT -H "Content-Type: application/json" -d '{
    "enabled": "true"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config
echo "##Set Repo level variables##"
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "AWS_ACCESS_KEY_ID",
    "value": "'$AWS_ACCESS_KEY_ID'",
    "secured": "true"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "AWS_SECRET_ACCESS_KEY",
    "value": "'$AWS_SECRET_ACCESS_KEY'",
    "secured": "true"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "S3_BUCKET_NAME",
    "value": "'$S3_BUCKET_NAME'",
    "secured": "false"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "S3_BUCKET_REGION",
    "value": "'$S3_BUCKET_REGION'",
    "secured": "false"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "S3_REMOTE_PATH",
    "value": "'$S3_REMOTE_PATH'",
    "secured": "false"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "REVIEVE_PARTNER_ID",
    "value": "'$REVIEVE_PARTNER_ID'",
    "secured": "false"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "FORM_V3_API_KEY",
    "value": "'$FORM_V3_API_KEY'",
    "secured": "false"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "AIBA_SERVICES_API_KEY",
    "value": "'$AIBA_SERVICES_API_KEY'",
    "secured": "false"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
curl -u $bitbucketUser:$bitbucketPasswordSecret -X POST -H "Content-Type: application/json" -d '{
    "key": "BASE_URL",
    "value": "'$BASE_URL'",
    "secured": "false"
    }
}' https://api.bitbucket.org/2.0/repositories/$workspace/$branchname/pipelines_config/variables/
echo "List of the files"
echo "List of the files"
ls -ltr
git init
git config --global user.name "$bitbucketUser"
git config --global user.password "$bitbucketPasswordSecret"
git config --global user.email "aiba_support_pbs_team_ind@publicissapient.com"
#Source Repo checkout
git clone --branch $sourceBranchName https://$bitbucketUser:$bitbucketPasswordSecret@bitbucket.org/$workspace/aiba-microfrontend.git
cd aiba-microfrontend
git checkout $sourceBranchName
git pull
echo "list of the files in Source Repo"
ls -ltr
# Deleting the files and folders which are not required for the rolled out repo
rm -f bitbucket-pipelines.yml
rm -rf packages/widget-api-tests/
rm -rf packages/widget-ui-configurator/
mv branddeploy.yml bitbucket-pipelines.yml
cd ..
# Dest Repo Checkout
git clone https://$bitbucketUser:$bitbucketPasswordSecret@bitbucket.org/$workspace/$branchname.git
echo "Copy the files in the $branchname repo"
cp ./aiba-microfrontend/.gitignore ./$branchname
cp ./aiba-microfrontend/.prettierignore ./$branchname
cp -rv ./aiba-microfrontend/* ./$branchname
cd $branchname
echo "list of files in $branchname repo after copy from source repo branch $sourceBranchName"
ls -ltr
#Update the configurator file before check in the code
echo "Update the configurator file before check in the code"
git add .
git status
git commit -m "Initial code commit "
echo "git branch"
git branch
echo "git remote -v"
git remote -v
echo "git push origin master"
git push origin master
ls -ltr

